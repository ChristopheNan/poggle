#  poggle

As we can consider that Google Translate uses open source to train, let's get 
some value back.

This script needs a Google Cloud Platform account. Use yours or create one.

Translated entries are marked as fuzzy and a sentinel mark is inserted at the
beginning of the translation. It is useful to distinguish them from fuzzy 
entries coming from `make merge`. Removing the sentinel mark removes 
automatically the fuzzy mark in my editor. 

## License

BSD 3-clause

Pull request are welcome.
