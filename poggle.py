#!/usr/bin/env python
"""
translate empty messages of .po files using Google Translate API
"""
import sys
from concurrent.futures import ThreadPoolExecutor
import polib
import argparse
import os
from typing import Any
from google.cloud import translate_v2 as translate
from rich.progress import Progress, TaskID
from html import unescape
import re

DEFAULT_TARGET = "fr"
DEFAULT_SENTINEL = "ggl:"
nb_bad_links = 0


def parse_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "--credentials",
        default=os.environ.get("GOOGLE_APPLICATION_CREDENTIALS"),
        help="Google Application credentials (if not set " "by Environment variable)",
    )
    parser.add_argument(
        "--target",
        default=DEFAULT_TARGET,
        help="Target language in ISO 639-1 language code (default: "
        f"{DEFAULT_TARGET})",
    )
    parser.add_argument(
        "--sentinel",
        default=DEFAULT_SENTINEL,
        help="insert sentinel characters to identify automatic translation "
        f"(default: '{DEFAULT_SENTINEL}')",
    )
    parser.add_argument(
        "filename",
        nargs="*",
        help=".po files to translate",
    )
    return parser.parse_args()


class Translator:
    def __init__(self, target: str, sentinel: str, credentials: Any = None):
        self.client = translate.Client(target_language=target, credentials=credentials)
        self.sentinel = sentinel
        self.nb_errors_link = 0

    def translate_entry(self, text: str) -> str:
        """Translates text from English to the target language.
        Adds a string at the beginning of the translated text
        to identify automatic translation.
        """
        result = self.client.translate(text, format_="text", source_language="en")
        return self.sentinel + result["translatedText"]

    def get_nb_errors_link(self):
        return self.nb_errors_link


def correct_rst_link(msg: str) -> str:
    """correct link format that are wrong from Google translation"""
    # TODO: remove function when problem fixed upstream
    global nb_bad_links
    pattern = r"`(.*[^ ])(<.*>) `"
    repl = r"`\1 \2`"
    res, cor = re.subn(pattern, repl, msg)
    nb_bad_links += cor
    return res


def translate_file(
    task_id: TaskID, filename: str, transl: Translator, progress: Progress
) -> None:
    try:
        pofile = polib.pofile(filename)
    except (OSError, UnicodeDecodeError) as e:
        progress.console.log(f"{filename} doesn't seem to be a .po file")
        return
    count = 0
    progress.update(task_id, total=len(pofile))
    for entry in pofile:
        if not entry.msgstr:
            text = transl.translate_entry(entry.msgid)
            # Google Translate inserts html characters and format of some links are bad
            entry.msgstr = correct_rst_link(unescape(text))
            entry.flags.append("fuzzy")
            count += 1
        progress.update(task_id, advance=1)
    if count > 0:
        pofile.save()
        progress.console.print(f"{filename}: {count} entries translated.")
    else:
        progress.console.print(f"{filename}: nothing translated")


def main():
    args = parse_args()
    translator = Translator(args.target, args.sentinel, args.credentials)
    with Progress() as progress, ThreadPoolExecutor() as pool:
        for fname in args.filename:
            task_id = progress.add_task(fname, start=False)
            pool.submit(translate_file, task_id, fname, translator, progress)
    print("bad links corrected:", nb_bad_links, file=sys.stderr)


if __name__ == "__main__":
    main()
